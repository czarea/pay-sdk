package com.czarea.pay.sdk.wxpay.config;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

import com.czarea.pay.sdk.wxpay.transport.IWxPayDomain;
import com.czarea.pay.sdk.wxpay.transport.WxPayDomainSimpleImpl;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 微信支付配置
 *
 * @author zhouzx
 */
@Data
@ConfigurationProperties(prefix = "wxpay")
@NoArgsConstructor
public class WxPayConfig {

    /**
     * appId
     */
    private Map<String, String> appId;
    /**
     * 商户号
     */
    private Map<String, String> mchId;
    /**
     * 商户密钥
     */
    private Map<String, String> key;
    /**
     * 证书路径
     */
    private String certPath = "E://微信开发//通证书//apiclient_cert.p12";

    /**
     * 账单下载Gzip文件保存路径
     */
    private String billPath = "E://微信开发//";

    /**
     * 超时时间
     */
    private long readTimeoutMs = 10000;
    /**
     * 连接超时时间
     */
    private long connectTimeoutMs = 2000;
    /**
     * 支付通知接收地址
     */
    private Map<String, String> notifyUrl;

    /**
     * 租户集合(php回调必须要租户id???)
     */
    private Map<String, String> tenantId;

    /**
     * 是否沙箱环境
     */
    private boolean useSandbox;
    /**
     * 是否上报请求信息
     */
    private boolean shouldAutoReport = false;
    /**
     * 上报最大队列
     */
    private int reportQueueMaxSize = 10000;

    /**
     * 主域名
     */
    private String primaryDomain = "api.mch.weixin.qq.com";
    /**
     * 候补域名
     */
    private String alternateDomain = "api2.mch.weixin.qq.com";
    /**
     * 报表线程数大小
     */
    private int reportWorkerNum = 1;
    /**
     * 报表批量大小
     */
    private int reportBatchSize = 2;

    /**
     * 刷卡支付查询间隔
     */
    private int micropayInterval = 10;

    /**
     * 刷卡支付查询总次数
     */
    private int micropayTimes = 3;

    private byte[] certData;

    public int getHttpConnectTimeoutMs() {
        return 2000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }

    public IWxPayDomain getWXPayDomain() {
        return WxPayDomainSimpleImpl.instance();
    }

    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

}
